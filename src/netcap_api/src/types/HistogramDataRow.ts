interface HistogramDataRow {
    IntervalStart: number;
    Count: number;
}
